/*
 * Copyright (C) 2010,2013-2015,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

/**
 * The Class ChangeableFileTest.
 */
public class ChangeableFileTest
{
	/**
     * 
     */
	@Test(expected = NullPointerException.class)
	public void test01()
	{
		new ChangeableFile((String) null);
	}

	/**
     * 
     */
	@Test
	public void test02()
	{
		ChangeableFile source = new ChangeableFile((File) null);

		Assert.assertNotNull(source);
		Assert.assertTrue(source.hasChanged());
		Assert.assertNull(source.getFile());
	}

	/**
     * 
     */
	@Test
	public void test03()
	{
		ChangeableFile source = new ChangeableFile("/tmp/foo");

		Assert.assertNotNull(source);
		Assert.assertTrue(source.hasChanged());
		source.getFile();
	}

	/**
     * 
     */
	@Test
	public void test04()
	{
		ChangeableFile source = new ChangeableFile("/tmp/foo");
	}
}
