/*
 * Copyright (C) 2010,2013-2015,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import org.junit.Assert;
import org.junit.Test;

/**
 * The Class EtcPasswdFileTest.
 */
public class EtcPasswdFileTest
{
	/**
     * 
     */
	@Test
	public void test01()
	{
		User user = EtcPasswdFile.instance().get(0);

		Assert.assertNotNull(user);
		Assert.assertEquals(0, user.getUid());
	}

}
