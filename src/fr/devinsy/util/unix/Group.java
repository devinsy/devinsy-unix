/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.util.Vector;

/**
 * The Class Group.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Group
{
	// static private final Logger logger =
	// LoggerFactory.getLogger(Group.class);

	/*
	 * /usr/include/grp.h
	 *
	 * The group structure.
	 *    struct group
	 *    {
	 *        char *gr_name;		  Group name.
	 *         char *gr_passwd;		 Password.
	 *         __gid_t gr_gid;		 Group ID.
	 *         char **gr_mem;		 Member list.
	 *    };
	 */

	private String name;
	private String password;
	private int gid;
	private Vector<String> members;

	/**
	 * Instantiates a new group.
	 */
	public Group()
	{
		this.name = null;
		this.password = null;
		this.gid = -1;
		this.members = new Vector<String>();
	}

	/**
	 * Adds the member.
	 * 
	 * @param login
	 *            the login
	 */
	public void addMember(final String login)
	{
		if ((login != null) && (login.length() != 0))
		{
			this.members.add(login);
		}
	}

	/**
	 * Gets the gid.
	 * 
	 * @return the gid
	 */
	public int getGid()
	{
		int result;

		result = this.gid;

		//
		return result;
	}

	/**
	 *
	 */
	public Vector<String> getMembers()
	{
		Vector<String> result;

		result = this.members;

		//
		return result;
	}

	/**
	 *
	 */
	public String getName()
	{
		String result;

		result = this.name;

		//
		return result;
	}

	/**
	 *
	 */
	public String getPassword()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 *
	 */
	public int gid()
	{
		int result;

		result = this.gid;

		//
		return result;
	}

	/**
	 *
	 */
	public Vector<String> members()
	{
		Vector<String> result;

		result = this.members;

		//
		return result;
	}

	/**
	 *
	 */
	public String name()
	{
		String result;

		result = this.name;

		//
		return result;
	}

	/**
	 *
	 */
	public String passwd()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 *
	 */
	public void setGid(final int gid)
	{
		this.gid = gid;
	}

	/**
	 *
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 *
	 */
	public void setPasswd(final String password)
	{
		this.password = password;
	}

	/**
	 *
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = "|" + this.name + "|" + this.password + "|" + this.gid + "|" + this.members + "|";

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////