/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

/**
 * The Class User.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class User
{
	// private static final Logger logger = LoggerFactory.getLogger(User.class);

	/*
	 * /usr/include/pwd.h
	 *
	 * The passwd structure.
	 *   struct passwd
	 *   {
	 *       char *pw_name;D                Username.  
	 *       char *pw_passwd;               Password.  
	 *        __uid_t pw_uid;               User ID.  
	 *        __gid_t pw_gid;               Group ID.  
	 *        char *pw_gecos;               Real name.  
	 *        char *pw_dir;                 Home directory.  
	 *        char *pw_shell;               Shell program.  
	 *   };
	 */

	private String login;
	private String password;
	private int uid;
	private int gid;
	private String realName;
	private String homeDirectory;
	private String shell;

	/**
	 * Instantiates a new user.
	 */
	public User()
	{
		this.login = null;
		this.password = null;
		this.uid = -1;
		this.gid = -1;
		this.realName = null;
		this.homeDirectory = null;
		this.shell = null;
	}

	/**
	 * Instantiates a new user.
	 * 
	 * @param user
	 *            the user
	 */
	public User(final User user)
	{
		this.login = user.login();
		this.password = user.passwd();
		this.uid = user.uid();
		this.gid = user.gid();
		this.realName = user.realName();
		this.homeDirectory = user.homeDirectory();
		this.shell = user.shell();
	}

	/**
	 * Gets the gid.
	 * 
	 * @return the gid
	 */
	public int getGid()
	{
		int result;

		result = this.gid;

		//
		return result;
	}

	/**
	 * Gets the home directory.
	 * 
	 * @return the home directory
	 */
	public String getHomeDirectory()
	{
		String result;

		result = this.homeDirectory;

		//
		return result;
	}

	/**
	 * Gets the login.
	 * 
	 * @return the login
	 */
	public String getLogin()
	{
		String result;

		result = this.login;

		//
		return result;
	}

	/**
	 * Gets the passwd.
	 * 
	 * @return the passwd
	 */
	public String getPasswd()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 * Gets the real name.
	 * 
	 * @return the real name
	 */
	public String getRealName()
	{
		String result;

		result = this.realName;

		//
		return result;
	}

	/**
	 * Gets the shell.
	 * 
	 * @return the shell
	 */
	public String getShell()
	{
		String result;

		result = this.shell;

		//
		return result;
	}

	/**
	 * Gets the uid.
	 * 
	 * @return the uid
	 */
	public int getUid()
	{
		int result;

		result = this.uid;

		//
		return result;
	}

	/**
	 * Gid.
	 * 
	 * @return the int
	 */
	public int gid()
	{
		int result;

		result = this.gid;

		//
		return result;
	}

	/**
	 * Home directory.
	 * 
	 * @return the string
	 */
	public String homeDirectory()
	{
		String result;

		result = this.homeDirectory;

		//
		return result;
	}

	/**
	 * Login.
	 * 
	 * @return the string
	 */
	public String login()
	{
		String result;

		result = this.login;

		//
		return result;
	}

	/**
	 * Passwd.
	 * 
	 * @return the string
	 */
	public String passwd()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 * Real name.
	 * 
	 * @return the string
	 */
	public String realName()
	{
		String result;

		result = this.realName;

		//
		return result;
	}

	/**
	 * Sets the gid.
	 * 
	 * @param gid
	 *            the new gid
	 */
	public void setGid(final int gid)
	{
		this.gid = gid;
	}

	/**
	 * Sets the home directory.
	 * 
	 * @param homeDirectory
	 *            the new home directory
	 */
	public void setHomeDirectory(final String homeDirectory)
	{
		this.homeDirectory = homeDirectory;
	}

	/**
	 * Sets the login.
	 * 
	 * @param login
	 *            the new login
	 */
	public void setLogin(final String login)
	{
		this.login = login;
	}

	/**
	 * Sets the passwd.
	 * 
	 * @param password
	 *            the new passwd
	 */
	public void setPasswd(final String password)
	{
		this.password = password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * Sets the real name.
	 * 
	 * @param realName
	 *            the new real name
	 */
	public void setRealName(final String realName)
	{
		this.realName = realName;
	}

	/**
	 * Sets the shell.
	 * 
	 * @param shell
	 *            the new shell
	 */
	public void setShell(final String shell)
	{
		this.shell = shell;
	}

	/**
	 * Sets the uid.
	 * 
	 * @param uid
	 *            the new uid
	 */
	public void setUid(final int uid)
	{
		this.uid = uid;
	}

	/**
	 * Shell.
	 * 
	 * @return the string
	 */
	public String shell()
	{
		String result;

		result = this.shell;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = "|" + this.login + "|" + this.password + "|" + this.uid + "|" + this.gid + "|" + this.realName + "|" + this.homeDirectory + "|" + this.shell + "|";

		//
		return result;
	}

	/**
	 * Uid.
	 * 
	 * @return the int
	 */
	public int uid()
	{
		int result;

		result = this.uid;

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////