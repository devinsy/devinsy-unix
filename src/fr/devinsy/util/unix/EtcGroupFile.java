/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.util.Vector;

import fr.devinsy.util.ChangeableFile;

/**
 * The Class EtcGroupFile.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class EtcGroupFile extends ChangeableFile
{
	// static private final Logger logger =
	// LoggerFactory.getLogger(EtcGroupFile.class);

	/** Lazy-loading with holder */
	private static class SingletonLoader
	{
		private static final EtcGroupFile instance = new EtcGroupFile();
	}

	private static EtcGroupFile instance = null;

	private Groups groups;

	/**
	 * Instantiates a new etc group file.
	 */
	private EtcGroupFile()
	{
		super("/etc/group");
		this.groups = new Groups();
	}

	/**
	 * This method checks if contains a target name.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public boolean contains(final String name)
	{
		boolean result;

		refresh();

		result = this.groups.contains(name);

		//
		return result;
	}

	/**
	 * Gets the.
	 * 
	 * @param gid
	 *            the gid
	 * @return the group
	 */
	public Group get(final int gid)
	{
		Group result;

		refresh();

		result = this.groups.getByGid(gid);

		//
		return result;
	}

	/**
	 * Gets a group by name.
	 * 
	 * @param name
	 *            the name
	 * @return the group
	 */
	public Group get(final String name)
	{
		Group result;

		refresh();

		result = this.groups.getByName(name);

		//
		return result;
	}

	/**
	 * Gets the login groups.
	 * 
	 * @param login
	 *            the login
	 * @return the login groups
	 */
	public Vector<String> getLoginGroups(final String login)
	{
		Vector<String> result;

		refresh();

		result = this.groups.getLoginGroups(login);

		//
		return result;
	}

	/**
	 * Gets the login groups string.
	 * 
	 * @param login
	 *            the login
	 * @return the login groups string
	 */
	public String getLoginGroupsString(final String login)
	{
		String result;

		refresh();

		result = this.groups.getLoginGroupsString(login);

		//
		return result;
	}

	/**
	 * This method returns the updated groups.
	 * 
	 * @return the groups
	 */
	@Override
	public void refresh()
	{
		if (hasChanged())
		{
			this.groups.clear();
			this.groups.addAll(EtcGroupFileReader.load());

			super.refresh();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = this.groups.toString();

		//
		return result;
	}

	/**
	 * This method returns the singleton instance.
	 * 
	 * @return the EtcGroupFile instance
	 */
	public static EtcGroupFile instance()
	{
		EtcGroupFile result;

		result = SingletonLoader.instance;

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////