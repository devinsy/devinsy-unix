/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.util.Iterator;
import java.util.Vector;

/**
 * The Class Groups.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Groups extends Vector<Group>
{

	private static final long serialVersionUID = 5802487312198869603L;

	// static private final Logger logger =
	// LoggerFactory.getLogger(Groups.class);

	/**
	 * Instantiates a new groups.
	 */
	public Groups()
	{
		super();
	}

	/**
	 * Contains.
	 * 
	 * @param gid
	 *            the gid
	 * @return true, if successful
	 */
	public boolean contains(final int gid)
	{
		boolean result;

		if (getByGid(gid) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Contains.
	 * 
	 * @param name
	 *            the name
	 * @return true, if successful
	 */
	public boolean contains(final String name)
	{
		boolean result;

		if (getByName(name) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 *
	 */
	public Group getByGid(final int gid)
	{
		Group result;

		result = null;
		boolean ended = false;
		Iterator<Group> iterator = this.iterator();
		while (!ended)
		{
			if (iterator.hasNext())
			{
				Group group = iterator.next();
				if (group.getGid() == gid)
				{
					ended = true;
					result = group;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by name.
	 * 
	 * @param name
	 *            the name
	 * @return the by name
	 */
	public Group getByName(final String name)
	{
		Group result;

		if (name == null)
		{
			result = null;
		}
		else
		{
			result = null;
			boolean ended = false;
			Iterator<Group> iterator = this.iterator();
			while (!ended)
			{
				if (iterator.hasNext())
				{
					Group group = iterator.next();
					if (group.getName().equals(name))
					{
						ended = true;
						result = group;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the login groups.
	 * 
	 * @param login
	 *            the login
	 * @return the login groups
	 */
	public Vector<String> getLoginGroups(final String login)
	{
		Vector<String> result;

		result = new Vector<String>();
		Iterator<Group> iterator = this.iterator();

		while (iterator.hasNext())
		{
			Group group = iterator.next();

			if (group.members().contains(login))
			{
				result.add(group.getName());
			}
		}

		//
		return result;
	}

	/**
	 * Gets the login groups string.
	 * 
	 * @param login
	 *            the login
	 * @return the login groups string
	 */
	public String getLoginGroupsString(final String login)
	{
		String result;

		Vector<String> groups = getLoginGroups(login);

		StringBuffer string = new StringBuffer();

		for (String group : groups)
		{
			if (string.length() == 0)
			{
				string.append(group);
			}
			else
			{
				string.append(",");
				string.append(group);
			}
		}

		result = string.toString();

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Vector#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		StringBuffer out = new StringBuffer();

		Iterator<Group> iterator = this.iterator();

		while (iterator.hasNext())
		{
			out.append(iterator.next().toString() + "\n");
		}

		result = out.toString();

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////