/*
 * Copyright (C) 2006-2010,2013-2015,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.io.File;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;
import fr.devinsy.util.strings.StringListUtils;
import fr.devinsy.util.unix.acl.Acl;
import fr.devinsy.util.unix.acl.AclManager;

/**
 * The Class Unix.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Unix
{
	private static final Logger logger = LoggerFactory.getLogger(Unix.class);
	public static final String SUDO = "/usr/bin/sudo";

	/**
	 * Append to file.
	 * 
	 * @param text
	 *            the text
	 * @param path
	 *            the path
	 * @throws Exception
	 *             the exception
	 */
	public static void appendToFile(final String text, final String path) throws UnixException
	{
		if ((text == null) || (text.length() == 0) || (path == null) || (path.length() == 0))
		{
			throw new IllegalArgumentException("Parameter undefined: [" + text + "][" + path + "].");
		}
		else
		{
			try
			{
				CmdExec.run(SUDO, "bash", "-c", "echo \"" + text + "\" >> " + path);
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error detected appending text to file [" + path + "].", exception);
			}
		}
	}

	/**
	 * Chmod.
	 * 
	 * @param changes
	 *            the changes
	 * @param path
	 *            the path
	 * @throws Exception
	 *             the exception
	 */
	public static void chmod(final String changes, final String path) throws UnixException
	{
		if ((changes == null) || (changes.length() == 0) || (path == null) || (path.length() == 0))
		{
			throw new IllegalArgumentException("Parameter undefined: [" + changes + "][" + path + "].");
		}
		else if (!new File(path).exists())
		{
			throw new IllegalArgumentException("Path not found: [" + path + "].");
		}
		else
		{
			try
			{
				CmdExec.run(SUDO, "chmod", changes, path);
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error running chmod command for [" + changes + "][" + path + "].", exception);
			}
		}
	}

	/**
	 * Clear acl.
	 * 
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @throws Exception
	 *             the exception
	 */
	public static void clearAcl(final String id, final String filePathName) throws UnixException
	{
		AclManager.clearId(id, filePathName);
	}

	/**
	 * Clear acl group.
	 * 
	 * @param group
	 *            the group
	 * @param filePathName
	 *            the file path name
	 * @throws Exception
	 *             the exception
	 */
	public static void clearAclGroup(final String group, final String filePathName) throws UnixException
	{
		AclManager.clearGroup(group, filePathName);
	}

	/**
	 * Clear acl user.
	 * 
	 * @param login
	 *            the login
	 * @param filePathName
	 *            the file path name
	 * @throws Exception
	 *             the exception
	 */
	public static void clearAclUser(final String login, final String filePathName) throws UnixException
	{
		AclManager.clearUser(login, filePathName);
	}

	/**
	 * Creates the user account.
	 * 
	 * @param login
	 *            the login
	 * @throws Exception
	 *             the exception
	 */
	public static void createUserAccount(final String login) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else
		{
			createUserAccount(login, login);
		}
	}

	/**
	 * Creates the user account.
	 * 
	 * @param login
	 *            the login
	 * @param name
	 *            the name
	 * @throws Exception
	 *             the exception
	 */
	public static void createUserAccount(final String login, final String name) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else if ((name == null) || (name.length() == 0))
		{
			throw new IllegalArgumentException("Name parameter undefined.");
		}
		else
		{
			createUserAccount(login, name, "/home/" + login);
		}
	}

	/**
	 * Creates the user account.
	 * 
	 * @param login
	 *            the login
	 * @param name
	 *            the name
	 * @param home
	 *            the home
	 * @throws Exception
	 *             the exception
	 */
	public static void createUserAccount(final String login, final String name, final String home) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else if ((name == null) || (name.length() == 0))
		{
			throw new IllegalArgumentException("Name parameter undefined.");
		}
		else if ((home == null) || (home.length() == 0))
		{
			throw new IllegalArgumentException("Home parameter undefined.");
		}
		else
		{
			try
			{
				logger.info("Creating user account for [" + login + "].");
				CmdExec.run(SUDO, "/usr/sbin/useradd", "-m", "-c", name, "-d", home, login);
				logger.info("User account created for [" + login + "].");
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error detected creating user account [" + login + "].", exception);
			}
		}
	}

	/**
	 * Creates the user account.
	 * 
	 * @param login
	 *            the login
	 * @param name
	 *            the name
	 * @param home
	 *            the home
	 * @param password
	 *            the password
	 * @throws Exception
	 *             the exception
	 */
	public static void createUserAccount(final String login, final String name, final String home, final String password) throws UnixException
	{
		if ((password == null) || (password.length() == 0))
		{
			throw new IllegalArgumentException("Password parameter undefined.");
		}
		else if (Unix.isLogin(login))
		{
			throw new IllegalArgumentException("Login [" + login + "] already in use");
		}
		else
		{
			createUserAccount(login, name, home);
			setPassword(login, password);
		}
	}

	/**
	 * Delete group.
	 * 
	 * @param group
	 *            the group
	 * @throws Exception
	 *             the exception
	 */
	public static void deleteGroup(final String group) throws UnixException
	{
		if ((group == null) || (group.length() == 0))
		{
			throw new IllegalArgumentException("Group parameter undefined.");
		}
		else
		{
			try
			{
				logger.info("Deleting group for [" + group + "].");
				CmdExec.run(SUDO + " groupdel " + group);
				logger.info("Group deleted for [" + group + "].");
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error running groupdel command for group [" + group + "].", exception);
			}
		}
	}

	/**
	 *
	 */
	public static void deleteUserAccount(final String login) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else
		{
			try
			{
				logger.info("Deleting user account for [" + login + "].");
				CmdExec.run(SUDO + " /usr/sbin/userdel " + login);
				logger.info("User account delted for [" + login + "].");
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error running userdel command for login [" + login + "].", exception);
			}
		}
	}

	/**
	 * Gets the acl data.
	 * 
	 * @param filePathName
	 *            the file path name
	 * @return the acl data
	 * @throws Exception
	 *             the exception
	 */
	public static String getAclData(final String filePathName) throws UnixException
	{
		String result;

		result = AclManager.getAclData(filePathName);

		//
		return result;
	}

	/**
	 * Gets the acl users.
	 * 
	 * @param filePathName
	 *            the file path name
	 * @return the acl users
	 * @throws Exception
	 *             the exception
	 */
	public static String[] getAclUsers(final String filePathName) throws UnixException
	{
		String[] result;

		Acl acl = AclManager.getAcl(filePathName);

		result = acl.currentAcl().getUserIds();

		//
		return result;
	}

	/**
	 * Checks if is group.
	 * 
	 * @param groupName
	 *            the group name
	 * @return true, if is group
	 */
	public static boolean isGroup(final String groupName)
	{
		boolean result;

		result = EtcGroupFile.instance().contains(groupName);

		//
		return result;
	}

	/**
	 * Checks if is login.
	 * 
	 * @param login
	 *            the login
	 * @return true, if is login
	 */
	public static boolean isLogin(final String login)
	{
		boolean result;

		result = EtcPasswdFile.instance().contains(login);

		//
		return result;
	}

	/**
	 * Link.
	 * 
	 * @param sourcePath
	 *            the source path
	 * @param targetPath
	 *            the target path
	 * @throws Exception
	 *             the exception
	 */
	public static void link(final String sourcePath, final String targetPath) throws UnixException
	{
		logger.info("[{}][{}]", sourcePath, targetPath);
		if ((sourcePath == null) || (sourcePath.length() == 0) || (targetPath == null) || (targetPath.length() == 0))
		{
			throw new IllegalArgumentException("Parameter undefined: [" + sourcePath + "][" + targetPath + "].");
		}
		else
		{
			File sourceFile = new File(sourcePath);
			File targetFile = new File(targetPath);

			if (!sourceFile.exists())
			{
				throw new IllegalArgumentException("Source does not exist: [" + sourcePath + "].");
			}
			else if ((targetFile.exists()) && (!targetFile.isDirectory()))
			{
				throw new IllegalArgumentException("Target already exists: [" + targetPath + "].");
			}
			else
			{
				try
				{
					CmdExec.run(SUDO, "ln", "-s", sourcePath, targetPath);
				}
				catch (CmdExecException exception)
				{
					throw new UnixException("Error detected linking [" + sourcePath + "][" + targetPath + "].", exception);
				}
			}
		}
	}

	/**
	 * Modify login.
	 * 
	 * @param sourceLogin
	 *            the source login
	 * @param targetLogin
	 *            the target login
	 * @param sourceHomeDirectory
	 *            the source home directory
	 * @throws Exception
	 *             the exception
	 */
	public static void modifyLogin(final String sourceLogin, final String targetLogin, final String sourceHomeDirectory) throws UnixException
	{
		logger.info("Starting login modifying: [" + sourceLogin + "] -> [" + targetLogin + "]");
		if ((sourceLogin == null) || (sourceLogin.length() == 0))
		{
			throw new IllegalArgumentException("Original login parameters undefined");
		}
		else if ((targetLogin == null) || (targetLogin.length() == 0))
		{
			throw new IllegalArgumentException("New login parameters undefined");
		}
		else if (!Unix.isLogin(sourceLogin))
		{
			throw new IllegalArgumentException("Original login unknow: [" + sourceLogin + "].");
		}
		else if (Unix.isLogin(targetLogin))
		{
			throw new IllegalArgumentException("New login unknow: [" + targetLogin + "].");
		}
		else if (sourceHomeDirectory == null)
		{
			throw new IllegalArgumentException("sourceHomeDirectory parameter undefined, thus no home directory move.");
		}
		else if (!new File(sourceHomeDirectory).exists())
		{
			throw new IllegalArgumentException("Source home directory does not exist: [" + sourceHomeDirectory + "].");
		}
		else
		{
			String targetHomeDirectory = new File(sourceHomeDirectory).getParent().toString() + "/" + targetLogin;
			if (new File(targetHomeDirectory).exists())
			{
				throw new IllegalArgumentException("Target home directory already exists: [" + targetHomeDirectory + "].");
			}
			else
			{
				try
				{
					logger.info("Login modifying: [" + sourceLogin + "] -> [" + targetLogin + "]");
					CmdExec.run(SUDO + " usermod -l " + targetLogin + " " + sourceLogin);
					logger.info("Login modified: [" + sourceLogin + "] -> [" + targetLogin + "]");
				}
				catch (CmdExecException exception)
				{
					throw new UnixException("Login modification failed for [" + sourceLogin + "].", exception);
				}

				// TOOD problem;
				try
				{
					logger.info("Renaming home directory: [" + sourceHomeDirectory + "] -> [" + targetLogin + "]");
					CmdExec.run(SUDO + " mv " + sourceHomeDirectory + " " + targetHomeDirectory);
					logger.info("Home directory renamed: [" + sourceHomeDirectory + "] -> [" + targetLogin + "]");
				}
				catch (CmdExecException exception)
				{
					throw new UnixException("Home directory rename failed for [" + sourceHomeDirectory + "].", exception);
				}
			}
		}
		logger.info("Login modifying done: [" + sourceLogin + "] -> [" + targetLogin + "]");
	}

	/**
	 * Recursive chmod.
	 * 
	 * @param changes
	 *            the changes
	 * @param path
	 *            the path
	 * @throws Exception
	 *             the exception
	 */
	public static void recursiveChmod(final String changes, final String path) throws UnixException
	{
		if ((changes == null) || (changes.length() == 0) || (path == null) || (path.length() == 0))
		{
			throw new IllegalArgumentException("Parameter undefined: [" + changes + "][" + path + "].");
		}
		else if (!new File(path).exists())
		{
			throw new IllegalArgumentException("Path not found: [" + path + "].");
		}
		else
		{
			try
			{
				CmdExec.run(SUDO, "chmod", "-R", changes, path);
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error running recursive chmod command for [" + changes + "][" + path + "].", exception);
			}
		}
	}

	/**
	 * Rename group.
	 * 
	 * @param sourceGroup
	 *            the source group
	 * @param targetGroup
	 *            the target group
	 * @throws UnixException
	 *             the unix exception
	 */
	public static void renameGroup(final String sourceGroup, final String targetGroup) throws UnixException
	{
		logger.info("Starting group renaming: [" + sourceGroup + "] -> [" + targetGroup + "]");
		if ((sourceGroup == null) || (sourceGroup.length() == 0))
		{
			throw new IllegalArgumentException("Original group name parameters undefined");
		}
		else if ((targetGroup == null) || (targetGroup.length() == 0))
		{
			throw new IllegalArgumentException("New group name parameters undefined");
		}
		else if (!Unix.isGroup(sourceGroup))
		{
			throw new IllegalArgumentException("Original group unknow: [" + sourceGroup + "].");
		}
		else if (Unix.isGroup(targetGroup))
		{
			throw new IllegalArgumentException("New group unknow: [" + targetGroup + "].");
		}
		else
		{
			try
			{
				logger.info("Login modifying: [" + sourceGroup + "] -> [" + targetGroup + "]");
				CmdExec.run(SUDO + " groupmod -n " + targetGroup + " " + sourceGroup);
				logger.info("Login modified: [" + sourceGroup + "] -> [" + targetGroup + "]");
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Group renaming failed for [" + sourceGroup + "].", exception);
			}
		}
		logger.info("Group renaming done: [" + sourceGroup + "] -> [" + targetGroup + "]");
	}

	/**
	 * Search group.
	 * 
	 * @param groupName
	 *            the group name
	 * @return the group
	 */
	public static Group searchGroup(final String groupName)
	{
		Group result;

		result = EtcGroupFile.instance().get(groupName);

		//
		return result;
	}

	/**
	 * Search login.
	 * 
	 * @param login
	 *            the login
	 * @return the user
	 */
	public static User searchLogin(final String login)
	{
		User result;

		result = EtcPasswdFile.instance().get(login);

		//
		return result;
	}

	/**
	 * Search login groups.
	 * 
	 * @param login
	 *            the login
	 * @return the vector
	 */
	public static Vector<String> searchLoginGroups(final String login)
	{
		Vector<String> result;

		result = EtcGroupFile.instance().getLoginGroups(login);

		//
		return result;
	}

	/**
	 * Sets the facl.
	 * 
	 * @param args
	 *            the new facl
	 * @throws Exception
	 *             the exception
	 */
	public static void setfacl(final String... args) throws UnixException
	{
		try
		{
			CmdExec.run(SUDO, "setfacl", args, 1, 6);
		}
		catch (CmdExecException exception)
		{
			throw new UnixException("Error running setfacl command for " + StringListUtils.toStringWithBrackets(args) + ":" + exception.getMessage() + ".", exception);
		}
	}

	/**
	 * As 'passwd' command has not the option '--stdin' in all systems (eg.
	 * Debian), this method uses the 'chpasswd' command.
	 * 
	 * @param login
	 *            the login
	 * @param newPassword
	 *            the new password
	 * @throws UnixException
	 *             the exception
	 */
	public static void setPassword(final String login, final String newPassword) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else if (newPassword == null)
		{
			throw new IllegalArgumentException("New password parameter undefined.");
		}
		else
		{
			try
			{
				logger.info("Password setting for [" + login + "].");
				CmdExec.run(SUDO, "bash", "-c", "echo \"" + login + ":" + newPassword + "\"| chpasswd ");
				logger.info("Password set for [" + login + "].");
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error detected on setting of the new password for [" + login + "].", exception);
			}
		}
	}

	/**
	 * chfn [ -f full-name ] [ username ].
	 * 
	 * @param login
	 *            the login
	 * @param newRealName
	 *            the new real name
	 * @throws UnixException
	 */
	public static void setRealName(final String login, final String newRealName) throws UnixException
	{
		if ((login == null) || (login.length() == 0))
		{
			throw new IllegalArgumentException("Login parameter undefined.");
		}
		else if (newRealName == null)
		{
			throw new IllegalArgumentException("New real name parameter undefined.");
		}
		else
		{
			try
			{
				logger.info("Real name changing for user [{}]", login);
				CmdExec.run(SUDO, "chfn", "-f", newRealName, login);
			}
			catch (CmdExecException exception)
			{
				throw new UnixException("Error detected on setting of the new real name for [" + login + "].", exception);
			}
		}
	}

	/**
	 * Unlink.
	 * 
	 * @param filePathName
	 *            the file path name
	 */
	public static void unlink(final String filePathName)
	{
		logger.info("[{}]", filePathName);
		if (filePathName == null)
		{
			throw new IllegalArgumentException("Parameter undefined: [" + filePathName + "].");
		}
		else
		{
			new File(filePathName).delete();
		}
	}
}

// ////////////////////////////////////////////////////////////////////////