/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class EtcGroupFileReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class EtcGroupFileReader
{
	private static final Logger logger = LoggerFactory.getLogger(EtcGroupFileReader.class);

	/**
	 * Load.
	 * 
	 * @return the groups
	 */
	public static Groups load()
	{
		Groups result;

		BufferedReader file = null;
		try
		{
			file = new BufferedReader(new FileReader("/etc/group"));

			result = new Groups();
			boolean ended = false;
			while (!ended)
			{
				String line = file.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					String[] tokens = line.split(":");

					Group group = new Group();
					group.setName(tokens[0]);
					group.setPassword(tokens[1]);
					group.setGid((new Integer(tokens[2])).intValue());

					// Manage the case of empty shell.
					if (tokens.length == 4)
					{
						String[] tokensBis = tokens[3].split(",");

						for (int tokenCounter = 0; tokenCounter < tokensBis.length; tokenCounter++)
						{
							group.addMember(tokensBis[tokenCounter]);
						}
					}
					result.add(group);
				}
			}
		}
		catch (java.io.IOException exception)
		{
			logger.error("IO error catched", exception);
			result = null;
		}
		finally
		{
			if (file != null)
			{
				try
				{
					file.close();
				}
				catch (IOException exception)
				{
					logger.error("Error closing.", exception);
				}
			}
		}

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////