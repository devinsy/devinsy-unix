/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.util.Iterator;
import java.util.Vector;

/**
 * The Class Users.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Users extends Vector<User>
{
	private static final long serialVersionUID = -7178304512851592399L;

	// private static final Logger logger =
	// LoggerFactory.getLogger(Users.class);

	/**
	 * Instantiates a new users.
	 */
	public Users()
	{
		super();
	}

	/* (non-Javadoc)
	 * @see java.util.Vector#addAll(java.util.Collection)
	 */
	public boolean addAll(final Users source)
	{
		boolean result;

		if (source == null)
		{
			result = true;
		}
		else
		{
			result = super.addAll(source);
		}

		//
		return result;
	}

	/**
	 * Contains.
	 * 
	 * @param uid
	 *            the uid
	 * @return true, if successful
	 */
	public boolean contains(final int uid)
	{
		boolean result;

		if (getByUid(uid) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Contains.
	 * 
	 * @param login
	 *            the login
	 * @return true, if successful
	 */
	public boolean contains(final String login)
	{
		boolean result;

		if (getByLogin(login) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Gets the by login.
	 * 
	 * @param login
	 *            the login
	 * @return the by login
	 */
	public User getByLogin(final String login)
	{
		User result;

		if (login == null)
		{
			result = null;
		}
		else
		{
			result = null;
			boolean ended = false;
			Iterator<User> iterator = this.iterator();
			while (!ended)
			{
				if (iterator.hasNext())
				{
					User user = iterator.next();
					if (user.getLogin().equals(login))
					{
						ended = true;
						result = user;
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by uid.
	 * 
	 * @param uid
	 *            the uid
	 * @return the by uid
	 */
	public User getByUid(final int uid)
	{
		User result;

		result = null;
		boolean ended = false;
		Iterator<User> iterator = this.iterator();
		while (!ended)
		{
			if (iterator.hasNext())
			{
				User user = iterator.next();
				if (user.getUid() == uid)
				{
					ended = true;
					result = user;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Vector#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		StringBuffer out;
		out = new StringBuffer();

		Iterator<User> iterator = this.iterator();

		while (iterator.hasNext())
		{
			out.append(iterator.next().toString() + "\n");
		}

		result = out.toString();

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////