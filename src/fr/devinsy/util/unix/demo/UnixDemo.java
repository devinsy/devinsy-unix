/*
 * Copyright (C) 2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix.demo;

import fr.devinsy.util.strings.StringList;

/**
 * The Class UnixDemo.
 */
public class UnixDemo
{
	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		// #1
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #1;");
			strings.appendln();

			System.out.println(strings.toString());
		}

		// #2
		{
			StringList strings = new StringList();

			strings.appendln("========== DEMO #2;");
			strings.appendln();

			System.out.println(strings.toString());
		}
	}
}
