/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.ChangeableFile;

/**
 * The Class EtcPasswdFile.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class EtcPasswdFile extends ChangeableFile
{
	/** Lazy-loading with holder */
	private static class SingletonLoader
	{
		private static final EtcPasswdFile instance = new EtcPasswdFile();
	}

	private static final Logger logger = LoggerFactory.getLogger(EtcPasswdFile.class);

	private Users users;

	/**
	 * Instantiates a new EtcPasswdFile class.
	 */
	private EtcPasswdFile()
	{
		super("/etc/passwd");
		this.users = new Users();
	}

	/**
	 * Contains.
	 * 
	 * @param login
	 *            the login
	 * @return true, if successful
	 */
	public boolean contains(final String login)
	{
		boolean result;

		refresh();

		result = this.users.contains(login);

		//
		return result;
	}

	/**
	 * Gets the.
	 * 
	 * @param uid
	 *            the uid
	 * @return the user
	 */
	public User get(final int uid)
	{
		User result;

		refresh();

		result = this.users.getByUid(uid);

		//
		return result;
	}

	/**
	 * Gets the.
	 * 
	 * @param login
	 *            the login
	 * @return the user
	 */
	public User get(final String login)
	{
		User result;

		refresh();

		result = this.users.getByLogin(login);

		//
		return result;
	}

	/**
	 * Updated users.
	 * 
	 * @return the users
	 */
	@Override
	public void refresh()
	{
		if (hasChanged())
		{
			this.users.clear();
			this.users.addAll(EtcPasswdFileReader.load());

			super.refresh();
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = this.users.toString();

		//
		return result;
	}

	/**
	 * Instance.
	 * 
	 * @return the EtcPasswdFile instance
	 */
	public static EtcPasswdFile instance()
	{
		EtcPasswdFile result;

		result = SingletonLoader.instance;

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////