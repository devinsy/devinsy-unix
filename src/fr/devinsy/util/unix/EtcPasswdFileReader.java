/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class EtcPasswdFileReader.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class EtcPasswdFileReader
{
	private static final Logger logger = LoggerFactory.getLogger(EtcPasswdFileReader.class);

	/**
	 * This method loads users from the /etc/password file.
	 * 
	 * @return the users
	 */
	public static Users load()
	{
		Users result;

		BufferedReader file = null;
		try
		{
			file = new BufferedReader(new FileReader("/etc/passwd"));

			result = new Users();

			boolean ended = false;
			while (!ended)
			{
				String line = file.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					String[] tokens = line.split(":");

					User user = new User();
					user.setLogin(tokens[0]);
					user.setPassword(tokens[1]);
					user.setUid((new Integer(tokens[2])).intValue());
					user.setGid((new Integer(tokens[3])).intValue());
					user.setRealName(tokens[4]);
					user.setHomeDirectory(tokens[5]);

					// Manage the case of empty shell.
					if (tokens.length == 7)
					{
						user.setShell(tokens[6]);
					}
					else
					{
						user.setShell("");
					}

					result.add(user);
				}
			}
		}
		catch (java.io.IOException exception)
		{
			logger.error("IO error catched", exception);
			result = null;
		}
		finally
		{
			if (file != null)
			{
				try
				{
					file.close();
				}
				catch (IOException exception)
				{
					logger.error("Error closing.", exception);
				}
			}
		}

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////