/*
 *  Copyright (C) 2016-2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix;

/**
 * The Class UnixException.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class UnixException extends Exception
{
	private static final long serialVersionUID = 5469332331366485779L;

	/**
	 * Instantiates a new UnixException.
	 */
	public UnixException()
	{
		super();
	}

	/**
	 * Instantiates a new UnixException.
	 * 
	 * @param message
	 *            the message
	 */
	public UnixException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new UnixException.
	 * 
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public UnixException(final String message, final Throwable cause)
	{
		super(message, cause);
	}

	/**
	 * Instantiates a new UnixException.
	 * 
	 * @param cause
	 *            the cause
	 */
	public UnixException(final Throwable cause)
	{
		super(cause);
	}
}
