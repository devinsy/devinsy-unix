/*
 * Copyright (C) 2010,2013-2015,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix.acl;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.cmdexec.CmdExec;
import fr.devinsy.util.cmdexec.CmdExecException;
import fr.devinsy.util.strings.StringListUtils;
import fr.devinsy.util.unix.Unix;
import fr.devinsy.util.unix.UnixException;

/**
 * The Class AclManager.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AclManager
{
	private static final Logger logger = LoggerFactory.getLogger(AclManager.class);
	public static final String SUDO = "/usr/bin/sudo";

	final static public Pattern USER_PATTERN = Pattern.compile("^user:(.*):(.*)$");
	final static public Pattern GROUP_PATTERN = Pattern.compile("^group:(.*):(.*)$");
	final static public Pattern MASK_PATTERN = Pattern.compile("^mask:(.*):(.*)$");
	final static public Pattern OTHER_PATTERN = Pattern.compile("^other:(.*):(.*)$");
	final static public Pattern DEFAULT_USER_PATTERN = Pattern.compile("^default:user:(.*):(.*)$");
	final static public Pattern DEFAULT_GROUP_PATTERN = Pattern.compile("^default:group:(.*):(.*)$");
	final static public Pattern DEFAULT_MASK_PATTERN = Pattern.compile("^default:mask:(.*):(.*)$");
	final static public Pattern DEFAULT_OTHER_PATTERN = Pattern.compile("^default:other:(.*):(.*)$");

	/**
	 * Clear group.
	 * 
	 * @param group
	 *            the group
	 * @param filePathName
	 *            the file path name
	 * @throws UnixException
	 *             the exception
	 */
	public static void clearGroup(final String group, final String filePathName) throws UnixException
	{
		if ((group == null) || (group.length() == 0) || (filePathName == null) || (filePathName.length() == 0))
		{
			throw new IllegalArgumentException("Bad parameters [" + group + "][" + filePathName + "].");
		}
		else
		{
			Unix.setfacl("-R", "-L", "-x", "group:" + group, filePathName);
			Unix.setfacl("-R", "-L", "-d", "-x", "group:" + group, filePathName);
		}
	}

	/**
	 * Clear id.
	 * 
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @throws UnixException
	 *             the exception
	 */
	public static void clearId(final String id, final String filePathName) throws UnixException
	{
		clearUser(id, filePathName);
		clearGroup(id, filePathName);
	}

	/**
	 * Clear user.
	 * 
	 * @param login
	 *            the login
	 * @param filePathName
	 *            the file path name
	 * @throws UnixException
	 *             the exception
	 */
	public static void clearUser(final String login, final String filePathName) throws UnixException
	{
		if ((login == null) || (login.length() == 0) || (filePathName == null) || (filePathName.length() == 0))
		{
			throw new IllegalArgumentException("Bad parameters [" + login + "][" + filePathName + "].");
		}
		else
		{
			Unix.setfacl("-R", "-L", "-x", "user:" + login, filePathName);
			Unix.setfacl("-R", "-L", "-d", "-x", "user:" + login, filePathName);
		}
	}

	/**
	 * Gets the acl.
	 * 
	 * @param filePathName
	 *            the file path name
	 * @return the acl
	 * @throws UnixException
	 */
	public static Acl getAcl(final String filePathName) throws UnixException
	{
		Acl result;

		result = new Acl(filePathName);

		String[] entries = getAclEntryLines(filePathName);

		// Login pattern: "^[a-z_][a-z0-9_-]*$".
		logger.debug("Line=[" + entries[1] + "]");
		Matcher matcher = Pattern.compile("^#\\sowner:\\s([a-z_][a-z0-9_-]*)$").matcher(entries[1]);
		if (matcher.matches())
		{
			logger.debug("group=[" + matcher.group(1) + "]");
			result.setOwner(matcher.group(1));
		}

		// Group pattern: "^[a-z_][a-z0-9_-]*$".
		logger.debug("Line=[" + entries[2] + "]");
		matcher = Pattern.compile("^#\\sgroup:\\s([a-z_][a-z0-9_-]*)$").matcher(entries[2]);
		if (matcher.matches())
		{
			logger.debug("group=[" + matcher.group(1) + "]");
			result.setOwner(matcher.group(1));
		}

		for (int nEntry = 3; nEntry < entries.length; nEntry++)
		{
			String entryLine = entries[nEntry];
			logger.debug("Line=[" + entryLine + "]");

			//
			Matcher userMatcher = USER_PATTERN.matcher(entryLine);
			Matcher groupMatcher = GROUP_PATTERN.matcher(entryLine);
			Matcher maskMatcher = MASK_PATTERN.matcher(entryLine);
			Matcher otherMatcher = OTHER_PATTERN.matcher(entryLine);
			Matcher defaultUserMatcher = DEFAULT_USER_PATTERN.matcher(entryLine);
			Matcher defaultGroupMatcher = DEFAULT_GROUP_PATTERN.matcher(entryLine);
			Matcher defaultMaskMatcher = DEFAULT_MASK_PATTERN.matcher(entryLine);
			Matcher defaultOtherMatcher = DEFAULT_OTHER_PATTERN.matcher(entryLine);

			AclEntry entry;
			if (userMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.USER, userMatcher.group(1), userMatcher.group(2));
				result.currentAcl().add(entry);
			}
			else if (groupMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.GROUP, groupMatcher.group(1), groupMatcher.group(2));
				result.currentAcl().add(entry);
			}
			else if (maskMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.MASK, maskMatcher.group(1), maskMatcher.group(2));
				result.currentAcl().add(entry);
			}
			else if (otherMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.OTHER, otherMatcher.group(1), otherMatcher.group(2));
				result.currentAcl().add(entry);
			}
			else if (defaultUserMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.USER, defaultUserMatcher.group(1), defaultUserMatcher.group(2));
				result.defaultAcl().add(entry);
			}
			else if (defaultGroupMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.GROUP, defaultGroupMatcher.group(1), defaultGroupMatcher.group(2));
				result.defaultAcl().add(entry);
			}
			else if (defaultMaskMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.MASK, defaultMaskMatcher.group(1), defaultMaskMatcher.group(2));
				result.defaultAcl().add(entry);
			}
			else if (defaultOtherMatcher.matches())
			{
				entry = new AclEntry(AclEntry.Type.OTHER, defaultOtherMatcher.group(1), defaultOtherMatcher.group(2));
				result.defaultAcl().add(entry);
			}
			else
			{
				throw new UnixException("Unknow ACL entry line pattern for [" + entryLine + "].");
			}

			logger.debug("Acl entry decoded: [" + entry.toString() + "]");
		}

		//
		return result;
	}

	/**
	 * Gets the ACL data.
	 * 
	 * @param filePathName
	 *            the file path name
	 * @return the acl data
	 */
	public static String getAclData(final String filePathName) throws UnixException
	{
		String result;

		try
		{
			logger.info("Getting Acl data for [" + filePathName + "].");
			result = CmdExec.run(SUDO, "/usr/bin/getfacl", "--no-effective", filePathName);
			logger.info("Acl data got for [" + filePathName + "].");
		}
		catch (CmdExecException exception)
		{
			throw new UnixException("Error getting ACL for [" + filePathName + "].", exception);
		}

		//
		return result;
	}

	/**
	 * Gets the ACL entry lines.
	 * 
	 * @param filePathName
	 *            the file path name
	 * @return the acl entry lines
	 */
	public static String[] getAclEntryLines(final String filePathName)
	{
		String[] result;

		try
		{
			result = getAclData(filePathName).split("\n");
		}
		catch (Exception exception)
		{
			result = new String[0];
		}

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @return true, if is used
	 * @throws UnixException
	 *             the exception
	 */
	public static boolean isUsed(final AclEntry.Type type, final String id, final String filePathName) throws UnixException
	{
		boolean result;

		result = isUsed(type, id, filePathName, 0);

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @param depth
	 *            the depth
	 * @return true, if is used
	 * @throws UnixException
	 * @throws Exception
	 *             the exception
	 */
	public static boolean isUsed(final AclEntry.Type type, final String id, final String filePathName, final int depth) throws UnixException
	{
		boolean result;

		if ((type == null) || (id == null) || (id.length() == 0) || (filePathName == null) || (filePathName.length() == 0) || (depth < 0))
		{
			throw new IllegalArgumentException("Bad parameter: [" + type + "][" + id + "][" + filePathName + "][" + depth + "].");
		}
		else
		{
			File file = new File(filePathName);
			if (!file.exists())
			{
				throw new IllegalArgumentException("File does not exist [" + filePathName + "].");
			}
			else
			{
				Acl acl = getAcl(filePathName);
				if (acl.contains(type, id))
				{
					result = true;
				}
				else if ((file.isDirectory()) && (depth > 0))
				{
					result = isUsed(type, id, filePathName, file.list(), depth - 1);
				}
				else
				{
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @param filePath
	 *            the file path
	 * @param filePathNames
	 *            the file path names
	 * @param depth
	 *            the depth
	 * @return true, if is used
	 * @throws UnixException
	 */
	public static boolean isUsed(final AclEntry.Type type, final String id, final String filePath, final String[] filePathNames, final int depth) throws UnixException
	{
		boolean result;

		result = false;
		boolean ended = false;
		int nLine = 0;
		while (!ended)
		{
			if (nLine < filePathNames.length)
			{
				String filePathName = filePathNames[nLine];
				if (isUsed(type, id, filePath + "/" + filePathName, depth))
				{
					ended = true;
					result = true;
				}
				else
				{
					nLine += 1;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @return true, if is used
	 * @throws Exception
	 *             the exception
	 */
	public static boolean isUsed(final String id, final String filePathName) throws Exception
	{
		boolean result;

		result = isUsed(id, filePathName, 0);

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param id
	 *            the id
	 * @param filePathName
	 *            the file path name
	 * @param depth
	 *            the depth
	 * @return true, if is used
	 * @throws Exception
	 *             the exception
	 */
	public static boolean isUsed(final String id, final String filePathName, final int depth) throws UnixException
	{
		boolean result;

		if ((id == null) || (id.length() == 0) || (filePathName == null) || (filePathName.length() == 0) || (depth < 0))
		{
			throw new IllegalArgumentException("Bad parameter: [" + id + "][" + filePathName + "][" + depth + "].");
		}
		else
		{
			File file = new File(filePathName);
			if (!file.exists())
			{
				throw new IllegalArgumentException("File does not exist [" + filePathName + "].");
			}
			else
			{
				Acl acl = getAcl(filePathName);
				if (acl.containsId(id))
				{
					result = true;
				}
				else if ((file.isDirectory()) && (depth > 0))
				{
					result = isUsed(id, file.list(), depth - 1);
				}
				else
				{
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Checks if is used.
	 * 
	 * @param id
	 *            the id
	 * @param filePathNames
	 *            the file path names
	 * @param depth
	 *            the depth
	 * @return true, if is used
	 * @throws UnixException
	 */
	public static boolean isUsed(final String id, final String[] filePathNames, final int depth) throws UnixException
	{
		boolean result;

		result = false;
		boolean ended = false;
		int nLine = 0;
		while (!ended)
		{
			if (nLine < filePathNames.length)
			{
				String filePathName = filePathNames[nLine];
				if (isUsed(id, filePathName, depth))
				{
					ended = true;
					result = true;
				}
				else
				{
					nLine += 1;
				}
			}
			else
			{
				ended = true;
				result = false;
			}
		}

		//
		return result;
	}

	/**
	 * Sets the facl.
	 * 
	 * @param args
	 *            the new facl
	 * @throws UnixException
	 *             the exception
	 */
	public static void setfacl(final String... args) throws UnixException
	{
		try
		{
			CmdExec.run(SUDO, "setfacl", args, 1, 5);
		}
		catch (CmdExecException exception)
		{
			throw new UnixException("Error running setfacl command for " + StringListUtils.toStringWithBrackets(args) + ":" + exception.getMessage() + ".");
		}
	}
}

// ////////////////////////////////////////////////////////////////////////