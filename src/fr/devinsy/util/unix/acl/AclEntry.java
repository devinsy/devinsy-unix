/*
 * Copyright (C) 2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix.acl;

/**
 * The Class AclEntry.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AclEntry
{
	// private static final Logger logger =
	// LoggerFactory.getLogger(AclEntry.class);

	/*
		user::rwx
		user:cpm:rwx                    #effective:rwx
		user:goo39:rwx                  #effective:rwx
		group::---                      #effective:---
		group:goo40:rwx                 #effective:rwx
		mask::rwx
		other::---
	 */

	public enum Type
	{
		NONE,
		USER,
		GROUP,
		MASK,
		OTHER
	};

	private Type type;
	private String id;
	private String permission;

	/**
	 * Instantiates a new acl entry.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @param permission
	 *            the permission
	 */
	public AclEntry(final Type type, final String id, final String permission)
	{
		this.type = type;
		this.id = id;
		this.permission = permission;
	}

	/**
	 * Id.
	 * 
	 * @return the string
	 */
	public String id()
	{
		String result;

		result = this.id;

		//
		return result;
	}

	/**
	 * Permission.
	 * 
	 * @return the string
	 */
	public String permission()
	{
		String result;

		result = this.type.toString() + ":" + this.id + ":" + this.permission;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = this.permission;

		//
		return result;
	}

	/**
	 * Type.
	 * 
	 * @return the type
	 */
	public Type type()
	{
		Type result;

		result = this.type;

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////