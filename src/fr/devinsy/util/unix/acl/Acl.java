/*
 * Copyright (C) 2010, 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix.acl;

/**
 * The Class Acl.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class Acl
{
	// private static final Logger logger = LoggerFactory.getLogger(Acl.class);

	/*
		# file: goo39
		# owner: goo39
		# group: goo39
		user::rwx
		user:cpm:rwx                    #effective:rwx
		user:goo39:rwx                  #effective:rwx
		group::---                      #effective:---
		group:goo40:rwx                 #effective:rwx
		mask::rwx
		other::---
		default:user::rwx
		default:user:cpm:rwx            #effective:rwx
		default:group::---              #effective:---
		default:group:cpm:rwx           #effective:rwx
		default:group:goo40:rwx         #effective:rwx
		default:mask::rwx
		default:other::---
	 */

	private String filePathname;
	private String owner;
	private String group;

	private AclEntries currentAcl;
	private AclEntries defaultAcl;

	/**
	 * Instantiates a new acl.
	 * 
	 * @param filePathname
	 *            the file pathname
	 */
	public Acl(final String filePathname)
	{
		this.filePathname = filePathname;
		this.owner = "";
		this.group = "";
		this.currentAcl = new AclEntries();
		this.defaultAcl = new AclEntries();
	}

	/**
	 * Contains.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	public boolean contains(final AclEntry.Type type, final String id)
	{
		boolean result;

		if ((this.currentAcl().contains(type, id)) || (this.defaultAcl().contains(type, id)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Contains group.
	 * 
	 * @param group
	 *            the group
	 * @return true, if successful
	 */
	public boolean containsGroup(final String group)
	{
		boolean result;

		result = contains(AclEntry.Type.GROUP, group);

		//
		return result;
	}

	/**
	 * Contains id.
	 * 
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	public boolean containsId(final String id)
	{
		boolean result;

		if ((containsUser(id) || containsGroup(id)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Contains user.
	 * 
	 * @param login
	 *            the login
	 * @return true, if successful
	 */
	public boolean containsUser(final String login)
	{
		boolean result;

		result = contains(AclEntry.Type.USER, login);

		//
		return result;
	}

	/**
	 * Current acl.
	 * 
	 * @return the acl entries
	 */
	public AclEntries currentAcl()
	{
		AclEntries result;

		result = this.currentAcl;

		//
		return result;
	}

	/**
	 * Default acl.
	 * 
	 * @return the acl entries
	 */
	public AclEntries defaultAcl()
	{
		AclEntries result;

		result = this.defaultAcl;

		//
		return result;
	}

	/**
	 * File pathname.
	 * 
	 * @return the string
	 */
	public String filePathname()
	{
		String result;

		result = this.filePathname;

		//
		return result;
	}

	/**
	 * Group.
	 * 
	 * @return the string
	 */
	public String group()
	{
		String result;

		result = this.group;

		//
		return result;
	}

	/**
	 * Owner.
	 * 
	 * @return the string
	 */
	public String owner()
	{
		String result;

		result = this.owner;

		//
		return result;
	}

	/**
	 * Sets the group.
	 * 
	 * @param group
	 *            the new group
	 */
	public void setGroup(final String group)
	{
		this.group = group;
	}

	/**
	 * Sets the owner.
	 * 
	 * @param owner
	 *            the new owner
	 */
	public void setOwner(final String owner)
	{
		this.owner = owner;
	}
}

// ////////////////////////////////////////////////////////////////////////