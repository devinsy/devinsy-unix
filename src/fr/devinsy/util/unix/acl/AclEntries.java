/*
 * Copyright (C) 2010, 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util.unix.acl;

import java.util.Iterator;
import java.util.Vector;

/**
 * The Class AclEntries.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class AclEntries extends Vector<AclEntry>
{
	private static final long serialVersionUID = 5802487312198869603L;

	// private static final Logger logger =
	// LoggerFactory.getLogger(AclEntries.class);

	/**
	 * Instantiates a new acl entries.
	 */
	public AclEntries()
	{
		super();
	}

	/**
	 * Contains.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	/*
	 *
	 */
	public boolean contains(final AclEntry.Type type, final String id)
	{
		boolean result;

		if (this.get(type, id) == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Contains id.
	 * 
	 * @param id
	 *            the id
	 * @return true, if successful
	 */
	public boolean containsId(final String id)
	{
		boolean result;

		if ((this.get(AclEntry.Type.USER, id) == null) && (this.get(AclEntry.Type.GROUP, id) == null))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Gets the.
	 * 
	 * @param type
	 *            the type
	 * @param id
	 *            the id
	 * @return the acl entry
	 */
	public AclEntry get(final AclEntry.Type type, final String id)
	{
		AclEntry result;

		result = null;
		boolean ended = false;
		Iterator<AclEntry> iterator = this.iterator();
		while (!ended)
		{
			if (iterator.hasNext())
			{
				AclEntry entry = iterator.next();
				if ((type == entry.type()) && (entry.id().equals(id)))
				{
					ended = true;
					result = entry;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Gets the group ids.
	 * 
	 * @return the group ids
	 */
	public String[] getGroupIds()
	{
		String[] result;

		Vector<String> ids = new Vector<String>();

		for (AclEntry entry : this)
		{
			if ((entry.type() == AclEntry.Type.GROUP) && (!ids.contains(entry.id())))
			{
				ids.add(entry.id());
			}
		}

		result = (String[]) ids.toArray();

		//
		return result;
	}

	/**
	 * Gets the ids.
	 * 
	 * @return the ids
	 */
	public String[] getIds()
	{
		String[] result;

		Vector<String> ids = new Vector<String>();

		for (AclEntry entry : this)
		{
			if (!ids.contains(entry.id()))
			{
				ids.add(entry.id());
			}
		}

		result = (String[]) ids.toArray();

		//
		return result;
	}

	/**
	 * Gets the user ids.
	 * 
	 * @return the user ids
	 */
	public String[] getUserIds()
	{
		String[] result;

		//
		Vector<String> ids = new Vector<String>();

		for (AclEntry entry : this)
		{
			if ((entry.type() == AclEntry.Type.USER) && (!ids.contains(entry.id())))
			{
				ids.add(entry.id());
			}
		}

		//
		result = new String[ids.size()];

		for (int nId = 0; nId < ids.size(); nId++)
		{
			result[nId] = ids.get(nId);
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Vector#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		StringBuffer out;
		out = new StringBuffer();

		Iterator<AclEntry> iterator = this.iterator();

		while (iterator.hasNext())
		{
			out.append(iterator.next().toString() + "\n");
		}

		result = out.toString();

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////