/*
 * Copyright (C) 2006-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-unix.
 * 
 * Devinsy-unix is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-unix is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-unix.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CachedFile.
 * 
 * @author Christian Pierre MOMON <christian.momon@devinsy.fr>
 */

public class ChangeableFile
{
	private static final Logger logger = LoggerFactory.getLogger(ChangeableFile.class);

	private File sourceFile;
	private long sourceTime;

	/**
	 * Instantiates a new cached file.
	 * 
	 * @param source
	 *            the file to cache
	 */
	public ChangeableFile(final File source)
	{
		this.sourceFile = source;
		this.sourceTime = 0;
	}

	/**
	 * Instantiates a new cached file.
	 * 
	 * @param fileName
	 *            the file name
	 */
	public ChangeableFile(final String fileName)
	{
		this(new File(fileName));
	}

	/**
	 * Gets the changeable file.
	 * 
	 * @return the changeable file
	 */
	public File getFile()
	{
		File result;

		result = this.sourceFile;

		//
		return result;
	}

	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public boolean hasChanged()
	{
		boolean result;

		if ((this.sourceFile == null) || (!this.sourceFile.exists()))
		{
			result = true;
		}
		else if (this.sourceTime == this.sourceFile.lastModified())
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/**
	 * Sets updated.
	 */
	public void refresh()
	{
		if ((this.sourceFile != null) && (this.sourceFile.exists()))
		{
			this.sourceTime = this.sourceFile.lastModified();
		}
	}

	/**
	 * Sets unload.
	 */
	public void unload()
	{
		this.sourceTime = 0;
	}
}

// ////////////////////////////////////////////////////////////////////////
